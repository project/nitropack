## Introduction
The [NitroPack](https://www.tinyurl.com/nitropack) module for Drupal is designed to optimize your website's performance by leveraging the power of [NitroPack](https://www.tinyurl.com/nitropack)'s all-in-one performance optimization service. This module provides a seamless integration between your Drupal site and [NitroPack](https://www.tinyurl.com/nitropack), allowing you to improve your site's load times, SEO rankings, and overall user experience.

### Features
The [NitroPack](https://www.tinyurl.com/nitropack) module offers a range of features including automatic image optimization, CSS/JS minification, and advanced caching. It's ideal for anyone looking to improve their site's performance without having to manually tweak each aspect of their site.

### Post-Installation
After installing the [NitroPack](https://www.tinyurl.com/nitropack) module, you can access its settings through the Drupal admin interface. From there, you can connect your [NitroPack](https://www.tinyurl.com/nitropack) account and configure the optimization settings to suit your needs.

### Additional Requirements
This module requires a [NitroPack](https://www.tinyurl.com/nitropack) account and the Drupal core. No additional modules or libraries are required.

### Recommended modules/libraries
While not required, the [NitroPack](https://www.tinyurl.com/nitropack) module works well with other performance optimization modules like Advanced CSS/JS Aggregation.

### Similar projects
While there are other performance optimization modules available, the [NitroPack](https://www.tinyurl.com/nitropack) module is unique in its comprehensive approach to performance optimization and its seamless integration with the [NitroPack](https://www.tinyurl.com/nitropack) service.

### Supporting this Module
If you find this module useful, you can support its development by contributing to our Patreon or OpenCollective.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION
- Add the Site ID
- Add the Site Secret Key

## MAINTAINERS

Current maintainers for Drupal 10:

- Ajith Thampi Joseph - https://www.drupal.org/u/thampiajit

