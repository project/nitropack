<?php

namespace Drupal\nitropack\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Configure Nitropack settings for this site.
 */
final class ConfigForm extends ConfigFormBase {

  /**
   * The RequestStack Class.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The ModuleHandler Class.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * ConfigForm constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The RequestStack Class.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The ModuleHandler Class.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The ConfigFactoryInterface Class.
   */
  public function __construct(RequestStack $request_stack,
    ModuleHandlerInterface $moduleHandler,
    ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
    $this->requestStack = $request_stack;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
        $container->get('request_stack'),
        $container->get('module_handler'),
        $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'nitropack_config';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['nitropack.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // Get the current configuration.
    $config = $this->config('nitropack.settings');

    // Get the base URL of the site.
    $base_url = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();

    // Add a disabled textfield for the Home URL.
    $form['home_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Home URL'),
      '#default_value' => $base_url,
      '#disabled' => TRUE,
    ];

    // Check if the Key module is enabled.
    if ($this->moduleHandler->moduleExists('key')) {
      // Get the keys from the Key module.
      $keys = \Drupal::service('key.repository')->getKeys();

      // Use the Key module for the site ID and site secret.
      $form['site_id'] = [
        '#type' => 'select',
        '#title' => $this->t('Site ID'),
        '#options' => $keys,
        '#default_value' => $config->get('site_id'),
        '#description' => $this->t('Please select the key for the Site ID.'),
      ];
      $form['site_secret'] = [
        '#type' => 'select',
        '#options' => $keys,
        '#title' => $this->t('Site Secret'),
        '#default_value' => $config->get('site_secret'),
        '#description' => $this->t('Please select the key for the Site Secret.'),
      ];
    }
    else {
      // Add textfields for the site ID and site secret.
      $form['site_id'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Site ID'),
        '#default_value' => $config->get('site_id'),
        '#description' => $this->t('Please enter the key for the Site ID.'),
      ];
      $form['site_secret'] = [
        '#type' => 'password',
        '#title' => $this->t('Site Secret'),
        '#default_value' => $config->get('site_secret'),
        '#description' => $this->t('Please enter the key for the Site Secret.'),
      ];
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // Get the values of the site ID and site secret fields.
    $site_id = $form_state->getValue('site_id');
    $site_secret = $form_state->getValue('site_secret');

    // Validate the site ID.
    if (empty($site_id)) {
      // Set an error for the form element with a corresponding name.
      $form_state->setErrorByName('site_id', $this->t('You must enter a site ID.'));
    }

    // Validate the site secret.
    if (empty($site_secret)) {
      $form_state->setErrorByName('site_secret', $this->t('You must enter a site secret.'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('nitropack.settings')
      ->set('home_url', $form_state->getValue('home_url'))
      ->set('site_id', $form_state->getValue('site_id'))
      ->set('site_secret', $form_state->getValue('site_secret'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
