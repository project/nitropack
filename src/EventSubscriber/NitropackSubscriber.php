<?php

namespace Drupal\nitropack\EventSubscriber;

use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Routing\AdminContext;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * NitroPack integration.
 */
class NitropackSubscriber implements EventSubscriberInterface {

  /**
   * The logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The admin context service.
   *
   * @var \Drupal\Core\Routing\AdminContext
   */
  protected $adminContext;

  /**
   * Constructs a new NitropackSubscriber.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Routing\AdminContext $admin_context
   *   The admin context service.
   */
  public function __construct(LoggerInterface $logger, ConfigFactoryInterface $config_factory, AdminContext $admin_context) {
    $this->logger = $logger;
    $this->configFactory = $config_factory;
    $this->adminContext = $admin_context;
  }

  /**
   * Kernel request event handler.
   */
  public function onKernelRequest(RequestEvent $event): void {
    // Skip admin routes.
    if ($this->adminContext->isAdminRoute()) {
      return;
    }

    // Get NitroPack configuration.
    $config = $this->configFactory->get('nitropack.settings');
    $home_url = $config->get('home_url');
    $site_id = $config->get('site_id');
    $site_secret = $config->get('site_secret');

    // Define NitroPack constants.
    if (!defined('NITROPACK_HOME_URL')) {
      define('NITROPACK_HOME_URL', $home_url);
    }
    if (!defined('NITROPACK_SITE_ID')) {
      define('NITROPACK_SITE_ID', $site_id);
    }
    if (!defined('NITROPACK_SITE_SECRET')) {
      define('NITROPACK_SITE_SECRET', $site_secret);
    }

    nitropack_handle_request();
    if (NULL !== $nitro = nitropack_get_instance()) {
      if ($nitro->isAllowedUrl($nitro->getUrl()) && $nitro->isAllowedRequest(TRUE)) {
        nitropack_init_webhooks();
        ob_start(function ($buffer) {
          if (nitropack_is_optimizer_request()) {
            // Flush registered tags.
            nitropack_add_tag(NULL, TRUE);
          }
          // Remove BOM from output.
          $bom = pack('H*', 'EFBBBF');
          $buffer = preg_replace("/^($bom)*/", '', $buffer);

          // Get the content type.
          $respHeaders = headers_list();
          $contentType = NULL;
          foreach ($respHeaders as $respHeader) {
            if (stripos(trim($respHeader), 'Content-Type:') === 0) {
              $contentType = $respHeader;
            }
          }

          // If the content type header was detected
          // and it's value does not contain 'text/html',
          // don't attach the beacon script.
          if ($contentType !== NULL && stripos($contentType, 'text/html') === FALSE) {
            return $buffer;
          }

          if (!preg_match("/<html.*?\s(amp|⚡)(\s|=|>)/", $buffer)) {
            $buffer = str_replace("</body", nitropack_get_beacon_script() . "</body", $buffer);
          }
            return $buffer;
        }, 0, PHP_OUTPUT_HANDLER_FLUSHABLE | PHP_OUTPUT_HANDLER_REMOVABLE);
      }
      else {
        header("X-Nitro-Disabled: 1");
      }
    }
  }

  /**
   * Adds Surrogate-Key header to cacheable master responses.
   *
   * @param \Symfony\Component\HttpKernel\Event\ResponseEvent $event
   *   The event to process.
   */
  public function onRespond(ResponseEvent $event) {
    if (!$event->isMainRequest()) {
      return;
    }

    $response = $event->getResponse();
    // Get the current URL.
    $url = $event->getRequest()->getUri();

    if ($response instanceof CacheableResponseInterface) {
      $tags = $response->getCacheableMetadata()->getCacheTags();

      // Rename all _list cache tags to _emit_list to avoid clearing list cache
      // tags by default.
      foreach ($tags as $tag) {
        $tag = str_replace('_list', '_emit_list', $tag);
        nitropack_sdk_purge($url, $tag, "Change triggered on the URL");
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::REQUEST => ['onKernelRequest', 300],
      KernelEvents::RESPONSE => ['onRespond', 300],

    ];
  }

}
